<?php

class ValidatePassword {
	
	const MIN_LENGTH = 6;
	const MAX_LENGTH = 20;
	
	public function validateLength($password) {
		$passlength = strlen($password);
		return $password >= self::MIN_LENGTH && $passlength <=self::MAX_LENGTH;
	}
	
	
}